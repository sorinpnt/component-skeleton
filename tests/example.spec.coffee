describe '[Example Test]', () ->
  
  scope = undefined
  ExampleCtrl = undefined
  createController = undefined
  locationParams = undefined
  beforeEach angular.mock.module 'component.example'
  
  #provider overloading
  beforeEach angular.mock.module ( _$provide_ ) ->
    $locationMock = {
      search : ( filters ) ->
        if filters?
          locationParams = filters
        locationParams
    }
    _$provide_.value '$location', $locationMock
    return

  #component creation
  beforeEach inject ( _$controller_, _$rootScope_ , _$location_) ->
    createController = () ->
      scope =  _$rootScope_ .$new()
      ExampleCtrl = _$controller_ 'ExampleCtrl', { $scope: scope, $location: _$location_ }

  beforeEach () ->
    locationParams = undefined
  
  afterEach () ->
    locationParams = undefined

  describe 'testing if controller is created properly -', () ->
    it "should create controller", () ->
      createController()
    
  describe 'testing controller functionality -', () ->
    it "should get current year", () ->
      expect(scope.getYear()).toEqual(2015)

    it "should get next year", () ->
      scope.incrementYear()
      expect(locationParams).toEqual( year : 2016 ) 
      expect(scope.getYear()).toEqual(2016)