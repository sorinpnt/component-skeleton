example = angular.module 'component.example', []

example.controller 'ExampleCtrl', [
  '$scope',
  '$location'
  ( $scope, $location ) ->
    year = 2015
    
    $scope.incrementYear = () ->
      year++
      $location.search { year: year }
    
    $scope.getYear = () ->
      year
]