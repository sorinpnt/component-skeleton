var gulp                  = require('gulp');
var jade                  = require('gulp-jade');
var less                   = require('gulp-less');
var coffee                = require('gulp-coffee');
var concat               = require('gulp-concat');
var flatten               = require('gulp-flatten');
var uglify                 = require('gulp-uglify');
var vendorsJson       = require('./vendor_libraries.json');
var vendorPathsJs    = [];
var vendorPathsCss  = [];

var createVendorPaths = function() {
  
  Object.keys(vendorsJson.js).forEach( function ( nodeModuleName ) {
    vendorsJson.js[nodeModuleName].forEach( function ( includedFileName ) { 
      vendorPathsJs.push( 'node_modules/' + nodeModuleName + '/**/' + includedFileName ) ;
    }); 
  });

  Object.keys(vendorsJson.css).forEach( function ( nodeModuleName ) {
    vendorsJson.css[nodeModuleName].forEach( function ( includedFileName ) { 
      vendorPathsCss.push( 'node_modules/' + nodeModuleName + '/**/' + includedFileName ) ;
    }); 
  });
};
createVendorPaths();

gulp.task('compile_vendor_js', function() {
  return gulp.src(vendorPathsJs)
             .pipe(flatten())
             .pipe(uglify())
             .pipe(concat('vendor.js'))
             .pipe(gulp.dest('./_temp/vendor/'));
});

gulp.task('compile_vendor_css', function() {
  return gulp.src(vendorPathsCss)
             .pipe(flatten())
             .pipe(concat('vendor.css'))
             .pipe(gulp.dest('./_temp/vendor/'));
});

gulp.task('compile_vendor_fonts', function() {
  return gulp.src(['node_modules/**/*.{otf,eot,svg,ttf,woff,woff2}'])
      .pipe(flatten())
      .pipe(gulp.dest('./_temp/vendor/'));
});

gulp.task('compile_less', function() {
  return gulp.src('src/**/*.less')
             .pipe(less())
             .pipe(flatten())
             .pipe(concat('custom.css'))
             .pipe(gulp.dest('./_temp/custom/'));
});

gulp.task('default', ['compile_vendor_js', 'compile_vendor_css',  'compile_vendor_fonts', 'compile_less']);