module.exports = function(config) {
  config.set({
    basePath: '../',
    frameworks: ['jasmine'],
    files: [ 
      '_temp/vendor/*.js', 
      '_temp/vendor/*.css', 
      '_temp/custom/*.css', 
      'src/**/*.coffee', 
      'tests/**/*.spec.coffee' 
      ],
    exclude: [],
    preprocessors: { 
      '**/*.coffee': ['coffee'],
      'src/**/*.coffee': ['coverage'] 
    },
    reporters: ['progress', 'coverage'],
    port: 9876,
    colors: true,
    logLevel: config.LOG_INFO,
    autoWatch: false,
    browsers: ['PhantomJS'],
    singleRun: true
  })
}
